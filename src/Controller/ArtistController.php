<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Form\ArtistType;
use App\Helpers\Api\Artist as ApiArtist;
use App\Repository\ArtistRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/artist")
 */
class ArtistController extends AbstractController
{
    /**
     * @Route("/", name="artist_index", methods={"GET"})
     */
    public function index(ArtistRepository $artistRepository): Response
    {

        try{
            #$this->denyAccessUnlessGranted('ROLE_SUPERADMIN');
        }catch(\Exception $e){
        #    var_dump($e->getMessage());die;
        }
        

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        

        #var_dump($this->getUser()->getRoles());die;


        return $this->render('artist/index.html.twig', [
            'artists' => $artistRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="artist_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $artist = new Artist();
        $form = $this->createForm(ArtistType::class, $artist);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * TODO: Validate If artist is valid from API
             */

             /*
             if($this->isValidArtist($artist)){

             }*/
            
            $artist->setCreatedAt(new DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($artist);
            $entityManager->flush();

            return $this->redirectToRoute('artist_index');
        }

        return $this->render('artist/new.html.twig', [
            'artist' => $artist,
            'form' => $form->createView(),
        ]);
    }



    private function isValidArtist($artist):bool{

        $api = new ApiArtist();

        if($artist->getId()){
            $api->setArtistId($artist->getId());
        }

        /**
         * TODO: Check API Execution
         */
        $api->execute();

        return true;

    }

    /**
     * @Route("/{id}", name="artist_show", methods={"GET"})
     */
    public function show(Artist $artist): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('artist/show.html.twig', [
            'artist' => $artist,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="artist_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Artist $artist): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(ArtistType::class, $artist);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $artist->setUpdatedAt(new DateTime());
            /**
             * TODO: Validate If artist is valid from API
             */

             /*
             if($this->isValidArtist($artist)){

             }*/
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('artist_index');
        }

        return $this->render('artist/edit.html.twig', [
            'artist' => $artist,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="artist_delete", methods={"POST"})
     */
    public function delete(Request $request, Artist $artist): Response
    {
        $is_admin = $this->isGranted('ROLE_ADMIN');

        if(!$is_admin){
            $this->addFlash('danger',"Only admins can delete albums");
            return $this->redirectToRoute('artist_index');
        }

        if ($this->isCsrfTokenValid('delete'.$artist->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($artist);
            $entityManager->flush();
        }

        return $this->redirectToRoute('artist_index');
    }
}
