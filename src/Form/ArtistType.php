<?php

namespace App\Form;

use App\Entity\Artist;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArtistType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null,[
                'attr'=>[
                    'class'=>'form-control mb-3'
                ]
            ])
            ->add('genre',ChoiceType::class,[
                'attr'=>[
                    'class'=>'form-control'
                ],
                'choices' => 
                array
                (
                    'pop'=>'Pop',
                    'Dance / EDM (Electronic Dance Music)'=>'Dance / EDM (Electronic Dance Music)',
                    'Hip-hop and Rap'=>'Hip-hop and Rap',
                    'R&B'=>'R&B',
                    'Latin'=>'Latin',
                    'Rock'=>'Rock',
                    'Metal'=>'Metal',
                    'Country'=>'Country',
                    'Folk (also called Contemporary folk - wikipedia)'=>'Folk (also called Contemporary folk - wikipedia)',
                    'Classical'=>'Classical',
                    'Jazz'=>'Jazz',
                    'Blues'=>'Blues',
                    'Easy Listening'=>'Easy Listening',
                    'New Age'=>'New Age',
                    'World'=>'World / Traditional Folk',
                    'Eletronic'=>'Eletronic'
                ) 
            ])
            ->add('status',null,[
                'attr'=>[
                    'class'=>'ml-2 mt-3'
                ]
            ])
            ->add('created_at',HiddenType::class,[

            ])
            ->add('updated_at',HiddenType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Artist::class,
        ]);
    }
}
