<?php 

namespace App\Helpers\Api;
class Artist {


    protected $url = 'https://moat.ai/api/task/';

    protected $method = 'GET';

    protected $body;

    protected $headers = [
        'Content-Type: application/json',
        'Authorization: Basic ZGV2ZWxvcGVyOlpHVjJaV3h2Y0dWeQ==',
        
    ];

    protected $artistId = null;


    public function execute(){
        $ch = curl_init();
        $url = $this->url;

        if(!empty($this->artistId)){
            $url .= $this->artistId;
        }

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->method);

        $headers = $this->headers;
        /**
         * TODO: Change hard coded credentials to environment variable
         */
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            #echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        try{
            return json_decode($result,true);
        }catch(\Exception $e){

            return [];
        }
        

    }


    /**
     * Get the value of url
     */ 
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of url
     *
     * @return  self
     */ 
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get the value of method
     */ 
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set the value of method
     *
     * @return  self
     */ 
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get the value of body
     */ 
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set the value of body
     *
     * @return  self
     */ 
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get the value of headers
     */ 
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Set the value of headers
     *
     * @return  self
     */ 
    public function setHeaders($headers)
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * Get the value of artistId
     */ 
    public function getArtistId()
    {
        return $this->artistId;
    }

    /**
     * Set the value of artistId
     *
     * @return  self
     */ 
    public function setArtistId($artistId)
    {
        $this->artistId = $artistId;

        return $this;
    }
}