![img](https://www.moat.ai/images/cropped-moat-logo-small.png) 
# Test Application

This test is a example of application to register and update entities of albums and artists, based in Symfony 5.

## Requirements
- php version: 7.1.3 or higher
PHP extensions: (all of them are installed and enabled by default in PHP 7+)
Ctype
iconv
JSON
PCRE
Session
SimpleXML
Tokenizer
- writable directories var/cache
- mysql 5.7
- nodejs v10 ou higher
- Docker (optional)

## Installation
RUN Command
```sh
mkdir <Any Directory to install the project>
cd <Any Directory to install the project>
git clone https://gitlab.com/adriano0488/moat-builder-test.git .
```

Check the file .env, 
```sh
Format: DATABASE_URL="mysql://<DATABASE_USERNAME>:<DATABASE_PASSWORD>@<DATABASE_HOST>:<DATABASE_PORT>/<DATABASE_NAME>"
DATABASE_URL="mysql://root:root@mysql:3306/api_db"
```
## Installation (Docker)
```sh
docker-compose build
docker-compose exec fpm php composer.phar install
docker-compose exec fpm npm install

#Compile assets (app.css and app.js) to public path (optional)
docker-compose exec fpm npm run dev

docker-compose exec fpm php bin/console doctrine:migrations:migrate -vvv
docker-compose up
```
```sh
You can access in Browser the URL http://localhost:81
```
The external exposed ports are:
```sh
HTTP (NGINX): port 81
PHP-FPM (NGINX): port 9000
MYSQL (NGINX): port 3307
```
## Notes
The project works without Docker, you need only to turn up a web server to public path of the project as DOCUMENT_ROOT and the database can be modified in .env file.

